/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50558
Source Host           : localhost:3306
Source Database       : mybatis

Target Server Type    : MYSQL
Target Server Version : 50558
File Encoding         : 65001

Date: 2020-07-15 14:28:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for my_chart01
-- ----------------------------
DROP TABLE IF EXISTS `my_chart01`;
CREATE TABLE `my_chart01` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `num` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of my_chart01
-- ----------------------------
INSERT INTO `my_chart01` VALUES ('1', '琛～', '5');
INSERT INTO `my_chart01` VALUES ('2', '缇婃瘺琛�', '20');
INSERT INTO `my_chart01` VALUES ('3', '闆汉琛�', '36');
INSERT INTO `my_chart01` VALUES ('4', '瑁ゅ瓙', '10');
INSERT INTO `my_chart01` VALUES ('5', '楂樿窡闉�', '10');
INSERT INTO `my_chart01` VALUES ('6', '琚滃瓙', '24');

-- ----------------------------
-- Table structure for my_chart02
-- ----------------------------
DROP TABLE IF EXISTS `my_chart02`;
CREATE TABLE `my_chart02` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `day` int(11) NOT NULL,
  `num` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of my_chart02
-- ----------------------------
INSERT INTO `my_chart02` VALUES ('1', '琛～', '1', '7');
INSERT INTO `my_chart02` VALUES ('2', '琛～', '2', '4');
INSERT INTO `my_chart02` VALUES ('3', '琛～', '3', '2');
INSERT INTO `my_chart02` VALUES ('4', '琛～', '4', '3');
INSERT INTO `my_chart02` VALUES ('5', '琛～', '5', '8');
INSERT INTO `my_chart02` VALUES ('6', '琛～', '6', '15');
INSERT INTO `my_chart02` VALUES ('7', '琛～', '7', '20');
INSERT INTO `my_chart02` VALUES ('8', '缇婃瘺琛�', '1', '5');
INSERT INTO `my_chart02` VALUES ('9', '缇婃瘺琛�', '2', '8');
INSERT INTO `my_chart02` VALUES ('10', '缇婃瘺琛�', '3', '12');
INSERT INTO `my_chart02` VALUES ('11', '缇婃瘺琛�', '4', '9');
INSERT INTO `my_chart02` VALUES ('12', '缇婃瘺琛�', '5', '25');
INSERT INTO `my_chart02` VALUES ('13', '缇婃瘺琛�', '6', '13');
INSERT INTO `my_chart02` VALUES ('14', '缇婃瘺琛�', '7', '8');
INSERT INTO `my_chart02` VALUES ('15', '琛～', '1', '2');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '鐢ㄦ埛ID',
  `username` varchar(50) NOT NULL COMMENT '鐢ㄦ埛鍚�',
  `age` int(11) NOT NULL COMMENT '骞撮緞',
  `sex` tinyint(3) NOT NULL COMMENT '0:淇濆瘑 1:鐢� 2:濂�',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'woaitianwen', '20', '0');
INSERT INTO `sys_user` VALUES ('2', 'yuyi', '18', '1');
INSERT INTO `sys_user` VALUES ('3', 'ceshi', '24', '2');
INSERT INTO `sys_user` VALUES ('4', 'ceshi02', '21', '2');

-- ----------------------------
-- Table structure for tb_book
-- ----------------------------
DROP TABLE IF EXISTS `tb_book`;
CREATE TABLE `tb_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_book
-- ----------------------------
INSERT INTO `tb_book` VALUES ('1', 'Spring MVC瀛︿範鎸囧崡', 'Paul Deck,Jack Dylan');
INSERT INTO `tb_book` VALUES ('4', 'Java楂樺苟鍙戠▼搴忚璁�', '钁涗竴楦�');
INSERT INTO `tb_book` VALUES ('5', '鏂囦綋涓ゅ紑鑺�', '绔犻噾鑾�');

-- ----------------------------
-- Table structure for tb_card
-- ----------------------------
DROP TABLE IF EXISTS `tb_card`;
CREATE TABLE `tb_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_card
-- ----------------------------
INSERT INTO `tb_card` VALUES ('1', '432801198009191038');

-- ----------------------------
-- Table structure for tb_clazz
-- ----------------------------
DROP TABLE IF EXISTS `tb_clazz`;
CREATE TABLE `tb_clazz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(18) DEFAULT NULL,
  `name` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_clazz
-- ----------------------------
INSERT INTO `tb_clazz` VALUES ('1', 'j1601', 'java灏变笟鐝�');
INSERT INTO `tb_clazz` VALUES ('2', 'j1602', '鍓嶇灏变笟鐝�');

-- ----------------------------
-- Table structure for tb_data
-- ----------------------------
DROP TABLE IF EXISTS `tb_data`;
CREATE TABLE `tb_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `fireDanger` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `temperature` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `humidity` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `speed` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `direction` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `rainfall` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `air_pressure` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `optical_radiation` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `ultraviolet_index` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `info` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_data
-- ----------------------------
INSERT INTO `tb_data` VALUES ('1', '1', 'no', '111c', '11', '11', '1', '1', null, null, null, null);
INSERT INTO `tb_data` VALUES ('2', '2', 'yes', '35c', '24', '2', '5', '5', '5', '5', '5', '5');

-- ----------------------------
-- Table structure for tb_device
-- ----------------------------
DROP TABLE IF EXISTS `tb_device`;
CREATE TABLE `tb_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `ip` varchar(100) COLLATE utf8_bin NOT NULL,
  `port` varchar(100) COLLATE utf8_bin NOT NULL,
  `protocol` varchar(100) COLLATE utf8_bin NOT NULL,
  `info` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_device
-- ----------------------------
INSERT INTO `tb_device` VALUES ('4', 'device004', '0.0.0.0', '0000', 'http', null, null);
INSERT INTO `tb_device` VALUES ('6', 'device001', '1.1.1.1', '1111', 'http', null, null);
INSERT INTO `tb_device` VALUES ('7', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('8', 'a', 'a', 'aa', 'aa', null, null);
INSERT INTO `tb_device` VALUES ('9', 'a', 'a', 'aa', 'a', null, null);
INSERT INTO `tb_device` VALUES ('10', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('11', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('12', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('13', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('14', 'a', 'aa', 'a', 'aa', null, null);
INSERT INTO `tb_device` VALUES ('15', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('16', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('17', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('18', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('19', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('20', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('21', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('22', 'a', 'a', 'a', '2', null, null);
INSERT INTO `tb_device` VALUES ('23', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('24', 'a', 'a', 'a', 'a', null, null);
INSERT INTO `tb_device` VALUES ('25', 'a', 'a', 'a', 'a', null, null);

-- ----------------------------
-- Table structure for tb_person
-- ----------------------------
DROP TABLE IF EXISTS `tb_person`;
CREATE TABLE `tb_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(18) DEFAULT NULL,
  `sex` varchar(18) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `card_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `card_id` (`card_id`) USING BTREE,
  CONSTRAINT `tb_person_ibfk_1` FOREIGN KEY (`card_id`) REFERENCES `tb_card` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_person
-- ----------------------------
INSERT INTO `tb_person` VALUES ('1', 'jack', '鐢�', '23', '1');

-- ----------------------------
-- Table structure for tb_student
-- ----------------------------
DROP TABLE IF EXISTS `tb_student`;
CREATE TABLE `tb_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(18) DEFAULT NULL,
  `sex` varchar(18) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `clazz_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `clazz_id` (`clazz_id`) USING BTREE,
  CONSTRAINT `tb_student_ibfk_1` FOREIGN KEY (`clazz_id`) REFERENCES `tb_clazz` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_student
-- ----------------------------
INSERT INTO `tb_student` VALUES ('1', '瀛︾敓1', '鐢�', '23', '1');
INSERT INTO `tb_student` VALUES ('2', '瀛︾敓2', '濂�', '18', '1');
INSERT INTO `tb_student` VALUES ('3', '瀛︾敓3', '鐢�', '20', '1');
INSERT INTO `tb_student` VALUES ('4', '瀛︾敓4', '濂�', '19', '2');
INSERT INTO `tb_student` VALUES ('5', '瀛︾敓5', '濂�', '18', '2');
INSERT INTO `tb_student` VALUES ('6', '瀛︾敓4', '鐢�', '24', '2');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'admin', '123456', '12345678900');
