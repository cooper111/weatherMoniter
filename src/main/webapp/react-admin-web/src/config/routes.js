

import { DeviceList } from '../components/pages/DeviceList';
import { DataList } from '../components/pages/DataList';

export const ROUTES = [
  {
    path: '/home',
    iconType: 'home',
    text: '数据列表',
    component: DataList,
  },
  {
    path: '/deviceList',
    iconType: 'order-list',
    text: '设备列表',
    component: DeviceList,
  },
];
