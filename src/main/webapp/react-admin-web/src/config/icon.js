import { HomeOutlined, OrderedListOutlined } from '@ant-design/icons';

export default {
  home: HomeOutlined,
  'order-list': OrderedListOutlined,
};
