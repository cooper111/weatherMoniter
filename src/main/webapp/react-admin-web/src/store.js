

import request, {get, post} from './utils/request';
import { client2server, resolveRespList } from './utils';
import { message as antdMsg } from 'antd';
import axios from 'axios';
import qs from 'qs';



export const login = (name, password) => {
  const url = '/user/login';
  const data = {
    name,
    password
  };

  // return post(url, data);
  return post(url, data).then(r => r.data.data);
};

export const checkIfLogin = (uid,token) => {
  const url = '/user/check';
  const data = {
    uid,
    token
  };
  return post(url, data);
};

export const getDeviceList = (params,token) => {
  const url = '/device/getAllDevice';
  return get(url, {
    token,
    ...params,
    ...client2server(params),
  }).then(r => resolveRespList(r.data));
};

export const resetPassword = (openId, password) => {
  const url = '/private/user/resetPassword';
  const data = {
    openId: openId,
    password: password,
  };
  const method = 'post';
  return request({
    method,
    data,
    url,
  }).then(r => r.data.content);
};

export function changeUserStatus(
  url = '/private/user/changeStatus',
  data = {},
  tip = '操作成功',
  config
) {
  return request({
    url,
    method: 'POST',
    data: data,
    ...config,
  }).then(r => {
    tip && antdMsg.success(tip);
    return r;
  });
}

export const getDataList = (params,token) => {
  const url = '/data/getAllData';
  return get(url, {
    token,
    ...params,
    ...client2server(params),
  }).then(r => resolveRespList(r.data));
};

export const getCardDetail = params => {
  const url = '/private/card/getCardById';
  return post(url, {
    ...params,
    ...client2server(params),
  }).then(r => r.data.content);
};
