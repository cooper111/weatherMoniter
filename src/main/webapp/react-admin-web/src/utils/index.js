export const DEFAULT_PAGE_SIZE = 20;

export function client2server({
  current = 1,
  pageSize = DEFAULT_PAGE_SIZE,
} = {}) {
  return {
    pageSize,
  };
}

export function server2client(content) {
  const { totalElements, size, number } = content || {};
  return {
    current: number + 1 || 1,
    total: totalElements || 0,
    pageSize: size || DEFAULT_PAGE_SIZE,
  };
}

export function resolveRespList(response) {

  console.log(response);
  const content = response.data;
  console.log(content);
  return {
    ...server2client(content),
    data: Array.isArray(content) ? content : [],
  };
}
