/**
 *
 * @param {{ token, uid}} info
 */
import {checkIfLogin} from "../store";

export var hasLogin = false;

export function saveAuth(info) {
  console.log(info);
  const { token, uid } = info;
  localStorage.setItem('token', token);
  localStorage.setItem('uid', uid);
}

/**
 * @returns {{ token, uid}}
 */
export function getAuth() {
  return {
    token: localStorage.getItem('token'),
    uid: localStorage.getItem('uid'),
  };
}

export function hasLogined() {
  return !!localStorage.getItem("token");
}

export function clearAuth() {
  localStorage.removeItem('token');
  localStorage.removeItem('uid');
}
