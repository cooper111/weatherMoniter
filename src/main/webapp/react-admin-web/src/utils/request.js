

import axios from 'axios';
import { message as antdMsg } from 'antd';
import { } from 'react-router-dom';
import { getAuth, clearAuth } from './auth';

const LOGIN_PAGE_PATH =
  process.env.NODE_ENV === 'development'
    ? '/#/login'
    : `${(process.env.REACT_APP_BASE_PATHNAME || '').replace(
      /\/$/,
      ''
    )}/#/login`;

//默认请求地址
axios.defaults.baseURL = 'http://127.0.0.1:8080/';

//处理跨域
axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8';

//默认请求时间
axios.defaults.timeout = 100000;

//处理登陆之后，接口依旧返回301
axios.defaults.withCredentials = false;

// 添加请求拦截器
axios.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    Promise.reject(error);
  }
);

// 添加响应拦截器
axios.interceptors.response.use(
  response => {
    const data = response?.data || {};
    const { code, message } = data;

    // 对响应数据做点什么
    if (code === 200) {
      // 不建议，会导致数据丢失
      // return Promise.resolve(response.data.content);
      return Promise.resolve(response);
    }

    antdMsg.error(message || '系统异常');
    return Promise.reject(response);
  },
  error => {
    const s = error?.response?.data.code;
    if (s === 400) {
      clearAuth();
      antdMsg.error('登录信息错误').promise.then(() => {
        window.location.replace(LOGIN_PAGE_PATH);
      });
    } else if (s) {
      antdMsg.error(`Http Error: ${s}`);
    }
    return Promise.reject(error);
  }
);

/**
 *
 * @param {import('axios').AxiosRequestConfig} config
 */
const request = config => {
  const authInfo = getAuth();
  config.headers = {
    Authorization: `Bearer ${authInfo.token}`,
    token: authInfo.token,
    ...config.headers,
  };
  return axios(config);
};

export default request;

/**
 * @param {string} url
 * @param {any} data
 * @param {import('axios').AxiosRequestConfig} config
 */
export function get(url = '', data = {}, config) {
  return request({
    url,
    method: 'GET',
    params: data,
    ...config,
  });
}

/**
 * @param {string} url
 * @param {any} data
 * @param {import('axios').AxiosRequestConfig} config
 */
export function post(url = '', data = {}, config) {
  return request({
    url,
    method: 'POST',
    data: data,
    ...config,
  });
}

/**
 * 提交操作类型接口，接口成功后自动提示
 * @param {string} url
 * @param {any} data
 * @param {string | false} tip
 * @param {import('axios').AxiosRequestConfig} config
 */
export function submitModify(url = '', data = {}, tip = '操作成功', config) {
  return request({
    url,
    method: 'POST',
    data: data,
    ...config,
  }).then(r => {
    tip && antdMsg.success(tip);
    return r;
  });
}
