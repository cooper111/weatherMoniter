/*
 * @Author: FBB
 * @Date: 2020-06-06 11:01:54
 * @LastEditors: FBB
 * @LastEditTime: 2020-06-06 11:09:51
 * @Description:
 */

import React from 'react';
import {getDataList, getDeviceList} from '../../store';
import { Space, Card, Popconfirm, Button } from 'antd';
import { submitModify } from '../../utils/request';
import ProTable from '@ant-design/pro-table';
import ImageViewer from '../common/ImageViewer';
import { DeviceAdd } from './DeviceAdd';

class ChangeStatus extends React.PureComponent {
  confirm = e => {
    submitModify('/private/card/changeStatus', {
      id: this.props.record.id,
      status: this.props.record.status === 1 ? 0 : 1,
    }).then(r => this.props.action.reload());
  };

  cancel = e => { };
  static defaultProps = {
    record: {},
    children: () => null,
    action: {
      reload: () => null,
    },
  };
  render() {
    return (
      <Popconfirm
        title={`是否${this.props.record.status === 1 ? '停用' : '启用'}该设备?`}
        onConfirm={this.confirm}
        onCancel={this.cancel}
        okText="确认"
        cancelText="取消"
      >
        <a>{this.props.record.status === 1 ? '停用' : '启用'}</a>
      </Popconfirm>
    );
  }
}

export class DataList extends React.Component {
  actionRef = React.createRef();

  /**
   * @type {import('@ant-design/pro-table').ProColumns[]}
   */
  columns = [
    {
      title: '设备ID',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: '火险',
      dataIndex: 'fireDanger',
      key: 'fireDanger',
      hideInSearch: true,
    },
    {
      title: '温度',
      dataIndex: 'temperature',
      key: 'temperature',
      hideInSearch: true,
    },
    {
      title: '湿度',
      dataIndex: 'humidity',
      key: 'humidity',
      hideInSearch: true,
    },
    {
      title: '风速',
      dataIndex: 'speed',
      key: 'speed',
      hideInSearch: true,
    },
    {
      title: '风向',
      dataIndex: 'direction',
      key: 'direction',
      hideInSearch: true,
    },
    {
      title: '雨量',
      dataIndex: 'rainfall',
      key: 'rainfall',
      hideInSearch: true,
    },
    // {
    //   title: '状态',
    //   dataIndex: 'status',
    //   key: 'status',
    //   initialValue: 'all',
    //   valueEnum: {
    //     all: { text: '全部', status: '' },
    //     0: {
    //       text: '停用',
    //       status: 0,
    //     },
    //     1: {
    //       text: '正常',
    //       status: 1,
    //     },
    //   },
    // },
    // {
    //   title: '操作',
    //   key: 'action',
    //   hideInSearch: true,
    //   render: (text, record, _, action) => (
    //     <Space size="middle">
    //       <ChangeStatus record={record} action={action}></ChangeStatus>
    //       <DeviceAdd record={record} action={{ current: action }}>
    //         {e => <a onClick={() => e.open('modify')}>修改</a>}
    //       </DeviceAdd>
    //     </Space>
    //   ),
    // },
  ];

  render() {
    return (
      <Card>
        <ProTable columns={this.columns} request={p => getDataList(p,localStorage.getItem("token"))} />
      </Card>
    );
  }
}
