
import React from 'react';
import {getDataList, getDeviceList} from '../../store';
import {Space, Form, Card, Input, Popconfirm, Button} from 'antd';
import Modal from 'antd/lib/modal/Modal';
import { submitModify } from '../../utils/request';
import ProTable from '@ant-design/pro-table';
import {DeviceAdd} from "./DeviceAdd";

class ResetPWD extends React.PureComponent {
  static defaultProps = {
    record: {},
    children: () => null,
    action: {
      reload: () => null,
    },
  };

  state = {
    show: false,
    loading: false,
  };

  /**
   * @type {React.RefObject<import('antd/lib/form').FormInstance>}
   */
  formRef = React.createRef();

  /**
   * @type {import('antd/lib/form').FormInstance}
   */
  get form() {
    return this.formRef.current;
  }

  handleOk = () => {
    this.setState({ loading: true });
    this.form
      .validateFields()
      .then(vals => {
        return submitModify('/private/user/resetPassword', {
          openId: this.props.record.id,
          password: vals.password,
        });
      })
      .then(() => {
        this.form.resetFields();
        this.close();
        this.props.action.reload();
      })
      .finally(() => {
        this.setState({ loading: false });
      });
  };

  handleCancel = () => {
    this.close();
  };

  open = () => {
    this.setState({ show: true });
  };

  close = () => {
    this.setState({ show: false });
  };

  render() {
    const { children } = this.props;
    const { show, loading } = this.state;
    return (
      <>
        {children({
          open: this.open,
        })}
        <Modal
          title="重置密码"
          visible={show}
          destroyOnClose={true}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          confirmLoading={loading}
        >
          <Form ref={this.formRef}>
            <Form.Item
              label="密码"
              name="password"
              rules={[{ required: true, message: '请输出密码!' }]}
            >
              <Input />
            </Form.Item>
          </Form>
        </Modal>
      </>
    );
  }
}

class ChangeStatus extends React.PureComponent {
  confirm = e => {
    submitModify('/device/delete', {
      id: this.props.record.id,
      status: this.props.record.status === 1 ? 0 : 1,
    }).then(r => this.props.action.reload());
  };

  cancel = e => { };
  static defaultProps = {
    record: {},
    children: () => null,
    action: {
      reload: () => null,
    },
  };
  render() {
    return (
      <Popconfirm
        title={`是否删除该设备?`}
        onConfirm={this.confirm}
        onCancel={this.cancel}
        okText="确认"
        cancelText="取消"
      >
        <a>{"删除"}</a>
      </Popconfirm>
    );
  }
}

export class DeviceList extends React.Component {
  /**
   * @type {import('@ant-design/pro-table').ProColumns[]}
   */

  columns = [
    {
      title: '设备ID',
      dataIndex: 'id',
      key: 'id',

    },
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
      hideInSearch: true,
    },
    {
      title: 'IP',
      dataIndex: 'ip',
      key: 'ip',
      hideInSearch: true,
    },
    {
      title: '端口',
      dataIndex: 'port',
      key: 'port',
      hideInSearch: true,
    },
    {
      title: '协议',
      dataIndex: 'protocol',
      key: 'protocol',
    },
    {
      title: '操作',
      key: 'action',
      hideInSearch: true,
      render: (text, record, _, action) => (
        <Space size="middle">
          <ChangeStatus record={record} action={action}></ChangeStatus>
        </Space>
      ),
    },
  ];

  render() {
    return (
        <Card>
            <ProTable
                actionRef={this.actionRef}
                columns={this.columns}
                request={p => getDeviceList(p)}
                toolBarRender={() => [
                  <DeviceAdd action={this.actionRef}>
                    {e => <Button onClick={() => e.open()}>添加设备</Button>}
                  </DeviceAdd>
                ]}
            />
          {/*<ProTable columns={this.columns} request={p => getDeviceList(p,localStorage.getItem("token"))} />*/}
        </Card>

    );
  }
}
