import React from 'react';
import { getCardDetail } from '../../store';
import { Form, Input } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import { submitModify } from '../../utils/request';
import { FORM_ITEM_LABEL_24 } from '../../utils/layout';

export class DeviceAdd extends React.PureComponent {
  static defaultProps = {
    record: {},
    children: () => null,
    action: {
      reload: () => null,
    },
  };

  state = {
    show: false,
    loading: false,
  };

  /**
   * @type {React.RefObject<import('antd/lib/form').FormInstance>}
   */
  formRef = React.createRef();

  /**
   * @type {import('antd/lib/form').FormInstance}
   */
  get form() {
    return this.formRef.current;
  }

  get action() {
    return this.props.action.current;
  }

  handleOk = () => {
    this.setState({ loading: true });
    this.form
      .validateFields()
      .then(vals => {
        return submitModify('/device/add', {
          ...vals,
        });
      })
      .then(() => {
        this.form.resetFields();
        this.close();
        // this.action.reload();
      })
      .finally(() => {
        this.setState({ loading: false });
      });
  };

  handleCancel = () => {
    this.close();
  };

  open = type => {
    this.setState({ show: true });
    if (type === 'modify') {
      getCardDetail({
        id: this.props.record.id,
      }).then(r => {
        console.info('detail', r);
        this.form.setFieldsValue({
          id: r.id,
          name: r.name,
          url: r.url,
          type: r.type,
        });
      });
    }
  };

  close = () => {
    this.setState({ show: false });
  };

  render() {
    const { children } = this.props;
    const { show, loading } = this.state;
    return (
      <>
        {children({
          open: this.open,
        })}
        <Modal
          title="添加设备"
          visible={show}
          destroyOnClose={true}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          confirmLoading={loading}
        >
          <Form ref={this.formRef}>
            <Form.Item {...FORM_ITEM_LABEL_24} label="ID" name="id">
              <Input disabled={true} />
            </Form.Item>
            <Form.Item
              {...FORM_ITEM_LABEL_24}
              label="设备名称"
              name="name"
              rules={[{ required: true, message: '请输入设备名称!' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
                {...FORM_ITEM_LABEL_24}
                label="IP"
                name="ip"
                rules={[{ required: true, message: '请输入设备IP!' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
                {...FORM_ITEM_LABEL_24}
                label="端口"
                name="port"
                rules={[{ required: true, message: '请输入端口号!' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
                {...FORM_ITEM_LABEL_24}
                label="协议"
                name="protocol"
                rules={[{ required: true, message: '请输入通信协议!' }]}
            >
              <Input />
            </Form.Item>
          </Form>
        </Modal>
      </>
    );
  }
}
