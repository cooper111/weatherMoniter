import React, { useMemo } from 'react';
import Viewer from 'viewerjs';
import 'viewerjs/dist/viewer.min.css';
import './style.scss';

const RcImage = props => {
  return <img className="img-viewer-img cp" alt="Error" {...props} />;
};

/**
 * 取得 src 后续作为图片的源链接
 * @param i
 */
function getSrcFromItem(i) {
  if (!i) {
    return '';
  }

  if (typeof i === 'string') {
    return i;
  }

  return i.src || '';
}

/**
 * @example
 *  <ImageViewer imgs={['1.jpg', '2.jpg']}} />;
 *  ImageViewer.show(['1.jpg', '2.jpg']);
 * @param {{ className; children; imgs; [k:string]: any }} props
 */
const ImageViewer = props => {
  const { className, children, imgs, ...rest } = props;
  const urls = useMemo(() => {
    // ! 空值 src 不能使用 ''，当第一张为 '' 时，会导致索引对应打开的图片不是预期的
    return imgs.map(_ => getSrcFromItem(_) || ' ');
  }, [imgs]);

  const open = (opts = {}) => {
    ImageViewer.show(urls, {
      ...rest,
      ...opts,
    });
  };

  const DefaultRender = useMemo(() => {
    if (children) {
      return null;
    }

    return imgs.map((_, i) => {
      if (typeof _ === 'string' || !_) {
        return (
          <RcImage
            key={`${i}_1_${_}`}
            src={_}
            onClick={() =>
              open({
                initialViewIndex: i,
              })
            }
          />
        );
      }

      return (
        <RcImage
          {..._}
          key={`${i}_2_${_.src}`}
          onClick={e => {
            if (typeof _.onClick === 'function') {
              _.onClick(e);
            }
            open({
              initialViewIndex: i,
            });
          }}
        />
      );
    });
  }, [children, imgs]);

  if (children) {
    return children({ open });
  }

  return <div className={className}>{DefaultRender}</div>;
};

/**
 *
 * @param {string | string[]} url
 * @param {import('viewerjs').Options} opts
 */
function show(url, opts = {}) {
  if (typeof url === 'string') {
    url = [url];
  }
  const imgsWrap = document.createElement('ul');
  imgsWrap.innerHTML = url.reduce((pre, next) => {
    if (next) {
      pre += `<img src="${next}" />`;
    }
    return pre;
  }, '');

  const gallery = new Viewer(imgsWrap, {
    ...opts,
    hide(event) {
      gallery.destroy();
      // tslint:disable-next-line: no-unused-expression
      opts.hide && opts.hide(event);
    },
  });

  gallery.show();
}

ImageViewer.show = show;

export default ImageViewer;
