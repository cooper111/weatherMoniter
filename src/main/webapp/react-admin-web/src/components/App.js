import React, { useEffect } from 'react';
import {
  HashRouter as Router,
  Route,
  useHistory,
  Switch,
  Redirect
} from 'react-router-dom';
import { Login } from './Login';
import { Home } from './Home';
import '../App.css';
import { ROUTES } from '../config/routes';
import { Result } from 'antd';

function C404() {
  const h = useHistory();
  return (
    <Result
      status="404"
      title="页面找不到了~"
      extra={<a onClick={() => h.replace('/')}>返回主页</a>}
    />
  );
}

const Index = () => {
  const h = useHistory();
  useEffect(() => {
    h.replace(ROUTES[0].path);
  }, []);
  return null;
};

function App() {
  return (
    <React.Fragment>
      <Router>
        <Switch>
          <Route exact path="/" component={Index} />
          <Route exact path="/login" component={Login} />
          {ROUTES.map(_ => (
            <Route
              exact
              key={_.path}
              path={_.path}
              render={props => {
                const component = _.component;
                return <Home>{React.createElement(component, props)}</Home>;
              }}
            />
          ))}
          <Route
            path="/*"
            render={() => (
              <Home>
                <C404 />
              </Home>
            )}
          />
        </Switch>
      </Router>
    </React.Fragment>
  )
}

export default App;
