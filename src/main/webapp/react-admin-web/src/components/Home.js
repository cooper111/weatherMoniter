

import React from 'react';
import { Layout, Menu, Avatar, Dropdown } from 'antd';
import {HashRouter as Router, Link, Route, Switch, withRouter, Redirect} from 'react-router-dom';
import { ROUTES } from '../config/routes';
import Icons from '../config/icon';
import {clearAuth} from '../utils/auth';
import { UserOutlined } from '@ant-design/icons';
import {Login} from "./Login";

const { Header, Content, Footer, Sider } = Layout;

export const Home = withRouter(
  class extends React.Component {
    state = {
      collapsed: false,
    };

    onCollapse = collapsed => {
      this.setState({ collapsed });
    };

    handleClickAvatarDropMenu = item => {
      const { key } = item;
      if (key === 'logout') {
        clearAuth();
        this.props.history.push('/login');
      }
    };

    render() {
      const { children, location } = this.props;
      const { collapsed } = this.state;
      const selectedKeys = [location.pathname];
      return(
        <Layout style={{ minHeight: '100vh' }}>
          <Sider collapsible collapsed={collapsed} onCollapse={this.onCollapse}>
            <div className="logo" />
            <Menu theme="dark" defaultSelectedKeys={selectedKeys} mode="inline">
              {ROUTES.map(route => {
                const I = Icons[route.iconType];
                return (
                  <Menu.Item key={route.path}>
                    {I && <I />}
                    {!collapsed && (
                      <Link to={route.path}>
                        <b>{route.text}</b>
                      </Link>
                    )}
                  </Menu.Item>
                );
              })}
            </Menu>
          </Sider>
          <Layout className="site-layout">
            <Header
              className="site-layout-background"
              style={{ textAlign: 'right' }}
            >
              <Dropdown
                overlay={
                  <Menu onClick={this.handleClickAvatarDropMenu}>
                    <Menu.Item key="logout">退出登录</Menu.Item>
                  </Menu>
                }
              >
                <Avatar className="cp" icon={<UserOutlined />} />
              </Dropdown>
            </Header>
            <Content style={{ margin: '0 16px' }}>
              <div
                className="site-layout-background"
                style={{ padding: 24, minHeight: 360 }}
              >
                {children}
              </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>
              {/*Ant Design ©2018 Created by Ant UED*/}
            </Footer>
          </Layout>
        </Layout>
    );
    }
  }
);
