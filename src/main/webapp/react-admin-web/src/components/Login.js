
import React, { useEffect } from 'react';
import { Form, Input, Button } from 'antd';
import { login } from '../store';
import { saveAuth, hasLogined } from '../utils/auth';
import { useHistory } from 'react-router-dom';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 12 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 14 },
};

export const Login = props => {
  const onFinish = values => {
    const { username, password } = values;

    login(username, password).then(res => {
      console.log("then");
      console.log(res);
      const uid = res.name;
      const token = res.token;
      saveAuth({ uid, token });
      props.history.push('/home');
    }).catch(res => {
      console.log("catch");
      console.log(res);
    });
  };
  const h = useHistory();

  // useEffect(() => {
  //   if (hasLogined()) {
  //     h.replace('/');
  //   }
  // }, []);

  return (
    <div
      style={{
        backgroundColor: '#ffffff',
        width: '500px',
        border: '1px solid #eee',
        padding: '80px 0 50px',
        margin: ' 200px auto 0',
        borderRadius: '5px',
      }}
    >
      <Form
        {...layout}
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <Form.Item
          label="用户名"
          name="username"
          rules={[{ required: true, message: '请输入用户名' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="密码"
          name="password"
          rules={[{ required: true, message: '请输入密码' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            登录
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
