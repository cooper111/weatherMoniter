package com.example.demo.waiter.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.core.Result;
import com.example.demo.core.ResultGenerator;
import com.example.demo.waiter.entity.TbUser;
import com.example.demo.waiter.service.ITbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiyou
 * @since 2020-02-09
 */
@RestController
@RequestMapping("/waiter/tb-user")
public class TbUserController {

    @Autowired
    private ITbUserService userService;

    @RequestMapping("/queryByName")
    public Result queryByName(@RequestParam("name")String name, @RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "5") Integer size) {
        Page<TbUser> pageInfo = new Page<>(page,size);
        IPage<TbUser> users = userService.selectPageVoByUserName(pageInfo, name);
        return ResultGenerator.genSuccessResult(users);
    }
}
