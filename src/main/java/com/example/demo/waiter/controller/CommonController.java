package com.example.demo.waiter.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.core.Result;
import com.example.demo.core.ResultGenerator;
import com.example.demo.waiter.entity.TbUser;
import com.example.demo.waiter.service.ITbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/waiter/common")
public class CommonController {

}