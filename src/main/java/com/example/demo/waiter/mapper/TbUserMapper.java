package com.example.demo.waiter.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.waiter.entity.TbUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiyou
 * @since 2020-02-09
 */
@Repository
public interface TbUserMapper extends BaseMapper<TbUser> {//可以继承或者不继承BaseMapper

    /**
     * 询 : 根据username查询用户列表，分页显示
     * @param page
     * @param name
     * @return
     */
    IPage<TbUser> selectPageVoByUserName(Page<?> page, @Param("name") String name);
}
