package com.example.demo.waiter.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.demo.waiter.entity.TbUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiyou
 * @since 2020-02-09
 */
public interface ITbUserService extends IService<TbUser> {
    IPage<TbUser> selectPageVoByUserName(Page<?> page, String name);
}
