package com.example.demo.core;

/**
 *
 * @author yanlv
 * @version 0.1 : BizCoreException v0.1 2016/11/25 下午9:48 yanlv Exp $
 */

public class BizCoreException extends RuntimeException {

    /**  */
    private static final long serialVersionUID = -5586792174208604173L;

    private ResultCode code;

    /**
     * 构造方法
     */
    public BizCoreException(String message) {
        super(message);
    }

    /**
     * 构造方法
     */
    public BizCoreException(String message, ResultCode code) {
        super(message);
        this.code = code;
    }


    /**
     * 构造方法
     */
    public BizCoreException(ResultCode code) {
        this.code = code;
    }

    /**
     * 构造方法
     */
    public BizCoreException(String message, Throwable e) {
        super(message, e);
    }

    /**
     * 构造方法
     */
    public BizCoreException(String message, ResultCode code, Throwable e) {
        super(message, e);
        this.code = code;
    }

    /**
     * 构造方法
     */
    public BizCoreException(Throwable e) {
        super(e);
    }

    /**
     * 构造方法
     */
    public BizCoreException(ResultCode code, Throwable e) {
        super(e);
        this.code = code;
    }

    public ResultCode getCode() {
        return code;
    }

    public void setCode(ResultCode code) {
        this.code = code;
    }
}
