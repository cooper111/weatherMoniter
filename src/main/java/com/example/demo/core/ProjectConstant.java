package com.example.demo.core;

/**
 * 项目常量
 */
public final class ProjectConstant {
    public static final String SUCCESS = "成功";
    public static final String FAIL = "失败";
    public static final String LOGIN_SUCCESS = "登陆成功";
    public static final String LOGIN_FAIL = "登陆失败";
//    public static final String BASE_PACKAGE = "com.example.demo";//基础包名称
//    public static final String MODEL_PACKAGE = BASE_PACKAGE + ".model";//Model所在包
//    public static final String MAPPER_PACKAGE = BASE_PACKAGE + ".dao";//Mapper所在包
//    public static final String SERVICE_PACKAGE = BASE_PACKAGE + ".service";//Service所在包
//    public static final String SERVICE_IMPL_PACKAGE = SERVICE_PACKAGE + ".impl";//ServiceImpl所在包
//    public static final String CONTROLLER_PACKAGE = BASE_PACKAGE + ".web";//Controller所在包
//
//    public static final String MAPPER_INTERFACE_REFERENCE = BASE_PACKAGE + ".core.Mapper";//Mapper插件基础接口的完全限定名
}
