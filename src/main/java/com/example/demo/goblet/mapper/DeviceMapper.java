package com.example.demo.goblet.mapper;

import com.example.demo.goblet.controller.form.DeviceForm;
import com.example.demo.goblet.entity.Device;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DeviceMapper {
    List<Device> getAllDevice();
    boolean addDevice(@Param("deviceForm") DeviceForm deviceForm);
    boolean delDevice(@Param("deviceForm") DeviceForm deviceForm);
    boolean updateDevice(@Param("deviceForm") DeviceForm deviceForm);
}
