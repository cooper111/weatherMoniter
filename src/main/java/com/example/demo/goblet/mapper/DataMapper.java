package com.example.demo.goblet.mapper;

import com.example.demo.goblet.entity.Data;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DataMapper {
    List<Data> getAllData();
}
