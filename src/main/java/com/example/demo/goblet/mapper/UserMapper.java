package com.example.demo.goblet.mapper;

import com.example.demo.goblet.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {
    User findByName(@Param("name") String name);
}
