package com.example.demo.goblet.manager;

import com.example.demo.goblet.controller.form.DeviceForm;
import com.example.demo.core.BizCoreException;
import com.example.demo.core.ResultCode;
import com.example.demo.goblet.entity.Device;
import com.example.demo.goblet.mapper.DeviceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Service
public class DeviceManager {

    @Resource
    DeviceMapper deviceMapper;

    public List<Device> getAllDevice(String token, HttpServletResponse response){
        if (!UserManager.checkToken(token)){
            response.setStatus(400);
            throw new BizCoreException("用户未登录", ResultCode.FAIL);
        }

        List<Device> allDevice = deviceMapper.getAllDevice();

        return allDevice;

    }

    public boolean addDevice(DeviceForm deviceForm, String token, HttpServletResponse response){
        if (!UserManager.checkToken(token)){
            response.setStatus(400);
            throw new BizCoreException("用户未登录", ResultCode.FAIL);
        }
        boolean res = deviceMapper.addDevice(deviceForm);

        return res;
    }

    public boolean delDevice(DeviceForm deviceForm, String token, HttpServletResponse response){
        if (!UserManager.checkToken(token)){
            response.setStatus(400);
            throw new BizCoreException("用户未登录", ResultCode.FAIL);
        }
        boolean res = deviceMapper.delDevice(deviceForm);

        return res;
    }

    public boolean updateDevice(DeviceForm deviceForm, String token, HttpServletResponse response){
        if (!UserManager.checkToken(token)){
            response.setStatus(400);
            throw new BizCoreException("用户未登录", ResultCode.FAIL);
        }
        boolean res = deviceMapper.updateDevice(deviceForm);

        return res;
    }
}
