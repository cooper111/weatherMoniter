package com.example.demo.goblet.manager;

import com.example.demo.core.BizCoreException;
import com.example.demo.core.ResultCode;
import com.example.demo.goblet.entity.User;
import com.example.demo.goblet.mapper.UserMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class UserManager {

    @Resource
    private UserMapper userMapper;


    public User login(String name, String password) {
        User user = userMapper.findByName(name);
        if (user == null) {
            throw new BizCoreException("该账号不存在", ResultCode.FAIL);
        }

        if (!user.getPassword().equals(password)) {
            throw new BizCoreException("密码错误", ResultCode.FAIL);
        }

        return user;
    }

    public static boolean checkToken(String token){
        if (token != null && token.equals("token")){
            return true;
        }else {
            return false;
        }
    }

}
