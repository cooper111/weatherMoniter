package com.example.demo.goblet.manager;

import com.example.demo.core.BizCoreException;
import com.example.demo.core.ResultCode;
import com.example.demo.goblet.entity.Data;
import com.example.demo.goblet.mapper.DataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


@Service
public class DataManager {
    @Resource
    DataMapper dataMapper;


    public List<Data>getAllData(String token, HttpServletResponse response){
        if (!UserManager.checkToken(token)){
            response.setStatus(400);
            throw new BizCoreException("用户未登录", ResultCode.FAIL);
        }

        List<Data> allData = dataMapper.getAllData();

        return allData;

    }
}
