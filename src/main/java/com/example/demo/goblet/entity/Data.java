package com.example.demo.goblet.entity;

import com.baomidou.mybatisplus.annotation.TableId;

@lombok.Data
public class Data {

    @TableId
    private Integer id;
    private Integer deviceId;
    private String fireDanger;
    private String temperature;
    private String humidity;
    private String speed;
    private String direction;
    private String rainfall;
    private String air_pressure;
    private String optical_radiation;
    private String ultraviolet_index;
    private String info;

}
