package com.example.demo.goblet.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

@Data
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Integer id;

    private String username;

    private String password;

    private String phone;
    
}
