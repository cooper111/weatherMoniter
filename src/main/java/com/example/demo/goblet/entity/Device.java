package com.example.demo.goblet.entity;

import lombok.Data;

@Data
public class Device {
    private Integer id;
    private String name;
    private String ip;
    private String port;
    private String protocol;
    private String info;
    private String type;
}
