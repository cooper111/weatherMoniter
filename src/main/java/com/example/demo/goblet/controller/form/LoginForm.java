package com.example.demo.goblet.controller.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginForm {

    @NotBlank(message = "账号不能为空")
    private String name;

    @NotBlank(message = "密码不能为空")
    private String password;
}
