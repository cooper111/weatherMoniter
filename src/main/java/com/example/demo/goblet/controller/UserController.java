package com.example.demo.goblet.controller;

import com.example.demo.goblet.controller.form.LoginForm;
import com.example.demo.goblet.controller.vo.UserInfoVo;
import com.example.demo.core.RestBusinessTemplate;
import com.example.demo.core.Result;
import com.example.demo.goblet.entity.User;
import com.example.demo.goblet.manager.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserManager userManager;

    @PostMapping("/login")
    public Result login(@RequestBody LoginForm loginForm, HttpServletResponse response){
        return RestBusinessTemplate.transaction(()->{
           //登录
           User user = userManager.login(loginForm.getName(), loginForm.getPassword());

           //生成token
            String token = "token";

            //返回
            response.setHeader("token",token);

            UserInfoVo userInfoVo = new UserInfoVo();
            userInfoVo.setId(user.getId());
            userInfoVo.setName(user.getUsername());
            userInfoVo.setPhone(user.getPhone());
            userInfoVo.setToken(token);

            return userInfoVo;
        });
    }

}
