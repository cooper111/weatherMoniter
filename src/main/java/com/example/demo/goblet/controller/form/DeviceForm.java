package com.example.demo.goblet.controller.form;

import lombok.Data;

@Data
public class DeviceForm {

    private Integer id;
    private String name;
    private String ip;
    private String port;
    private String protocol;
}
