package com.example.demo.goblet.controller.vo;


import lombok.Data;

@Data
public class UserInfoVo {
    private String name;

    private Integer id;

    private String token;

    private String phone;
}
