package com.example.demo.goblet.controller;


import com.example.demo.core.RestBusinessTemplate;
import com.example.demo.core.Result;
import com.example.demo.goblet.manager.DataManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
@RequestMapping("/data")
public class DataController {

    @Autowired
    DataManager dataManager;

    @GetMapping("/getAllData")
    public Result getAllData(HttpServletRequest request, HttpServletResponse response){

        //分页查询current pageSize
        String params = request.getQueryString();
        String token = request.getHeader("token");
        return RestBusinessTemplate.execute(()->{
            return dataManager.getAllData(token,response);
        });
    }
}
