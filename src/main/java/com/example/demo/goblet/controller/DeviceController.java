package com.example.demo.goblet.controller;

import com.example.demo.goblet.controller.form.DeviceForm;
import com.example.demo.core.RestBusinessTemplate;
import com.example.demo.core.Result;
import com.example.demo.goblet.manager.DeviceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@CrossOrigin
@RestController
@RequestMapping("/device")
public class DeviceController {
    @Autowired
    DeviceManager deviceManager;

    @GetMapping("/getAllDevice")
    public Result getAllDevice(HttpServletRequest request, HttpServletResponse response){

        //分页查询current pageSize
        String params = request.getQueryString();
        String token = request.getHeader("token");
        return RestBusinessTemplate.execute(()-> deviceManager.getAllDevice(token,response));
    }

    @PostMapping("/add")
    public Result addDevice(@RequestBody DeviceForm deviceForm, HttpServletRequest request, HttpServletResponse response){

        String token = request.getHeader("token");
        return RestBusinessTemplate.execute(()-> deviceManager.addDevice(deviceForm, token, response));
    }

    @PostMapping("/delete")
    public Result delDevice(@RequestBody DeviceForm deviceForm, HttpServletRequest request, HttpServletResponse response){

        String token = request.getHeader("token");
        return RestBusinessTemplate.execute(()-> deviceManager.delDevice(deviceForm, token, response));
    }

    @PostMapping("/update")
    public Result updateDevice(@RequestBody DeviceForm deviceForm, HttpServletRequest request, HttpServletResponse response){

        String token = request.getHeader("token");
        return RestBusinessTemplate.execute(()-> deviceManager.updateDevice(deviceForm, token, response));
    }
}
