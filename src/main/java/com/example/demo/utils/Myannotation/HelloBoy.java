package com.example.demo.utils.Myannotation;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @ClassName HelloBoy
 * @Description
 * @Company inspur
 * @Author Kevin
 * @Date 2020/7/12 19:17
 * @Version 1.0
 */
@Service
public class HelloBoy {

    @Boy("Kevin")
    String name = "world";

    public void sayHello() {
        System.out.println("hello, " + name);
    }
}
