package com.example.demo.utils.Myannotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @ClassName Boy
 * @Description 自定义注解接口
 * @Company inspur
 * @Author Kevin
 * @Date 2020/7/12 19:06
 * @Version 1.0
 */

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented//Java Doc
@Component
public @interface Boy {
    String value() default "";
}
