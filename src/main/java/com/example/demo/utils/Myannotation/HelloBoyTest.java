package com.example.demo.utils.Myannotation;

import com.example.demo.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @ClassName HelloBoyTest
 * @Description Boy注解测试类
 * @Company inspur
 * @Author Kevin
 * @Date 2020/7/12 19:21
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DemoApplication.class})
public class HelloBoyTest {
    @Autowired
    private HelloBoy helloBoy;

    @Test
    public void HelloBoyTest() {
        helloBoy.sayHello();
    }
}
