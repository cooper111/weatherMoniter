package com.example.demo.netty.client.bio;

/**
 * @author Kevin
 * @date 2020/4/16 11:37
 */
public class ClientTest {
    public static void main(String[] args) {
        //需要服务器的正确的IP地址和端口号
        Client client = new Client("localhost", 8000);//6768
        client.start();
    }
}
