package com.example.demo.netty.util;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName SessionUtil
 * @Description
 * @Company inspur
 * @Author Kevin
 * @Date 2020/7/13 22:43
 * @Version 1.0
 */
public class SessionUtil {
    public static final Map<Byte, ChannelHandlerContext> equipmentChannelMap = new ConcurrentHashMap<>();

}
