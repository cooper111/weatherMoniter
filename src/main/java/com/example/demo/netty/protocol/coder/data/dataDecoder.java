package com.example.demo.netty.protocol.coder.data;

import com.example.demo.netty.protocol.model.data.modBusData;
import io.netty.buffer.ByteBuf;

/**
 * @ClassName dataDecoder
 * @Description
 * @Company inspur
 * @Author Kevin
 * @Date 2020/7/4 12:13
 * @Version 1.0
 */
public class dataDecoder {
    public static final dataDecoder INSTANCE = new dataDecoder();
    private dataDecoder(){};

    public modBusData decode(ByteBuf byteBuf) {
        modBusData data = new modBusData();
        data.setAddress(byteBuf.readByte());//地址码
        data.setFunc(byteBuf.readByte());//功能码
        //byte转int时，需要&0xff，补足前面的24位。因此如果byte是负数，转成int时可能会变成整数
        data.setByteNum(byteBuf.readByte()& 0xff);//字节数  1字节！
        data.setRegisterVal(byteBuf.readBytes(2 * (int)(byteBuf.readByte()& 0xff)).array());//寄存器值
        data.setCrc(byteBuf.readBytes(2).array());//CRC校验
        return data;
    }
}
