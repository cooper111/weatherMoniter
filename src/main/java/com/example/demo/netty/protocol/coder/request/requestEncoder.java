package com.example.demo.netty.protocol.coder.request;

import com.example.demo.netty.protocol.model.request.modbusRequest;
import com.example.demo.netty.util.convert;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

import javax.xml.bind.DatatypeConverter;
import java.nio.ByteBuffer;
import java.util.List;

/**
 * @ClassName requestEncoder
 * @Description
 * @Company inspur
 * @Author Kevin
 * @Date 2020/7/4 10:55
 * @Version 1.0
 */
public class requestEncoder {

    public static final requestEncoder INSTANCE = new requestEncoder();
    private requestEncoder() {}

    public ByteBuf encode(ByteBuf byteBuf, modbusRequest packet) {
//        // 1. 创建 ByteBuf 对象
//        ByteBuf byteBuf = byteBufAllocator.ioBuffer();
        // 2. 实际编码过程
        byteBuf.writeByte(packet.getAddress());//地址码
        byteBuf.writeByte(packet.getFunc());//功能码
        byteBuf.writeBytes(packet.getStartAddress());//起始地址
        byteBuf.writeBytes(convert.INSTANCE.numToBytes(packet.getRegisterNum()));//寄存器数量
        byteBuf.writeBytes(packet.getCrc());//CRC校验

        return byteBuf;
    }

    public ByteBuffer encode2ByteBuffer(modbusRequest packet) {
        //modBus请求固定为8字节
        byte[] req = new byte[8];
        //地址码
        req[0] = packet.getAddress();
        //功能码
        req[1] = packet.getFunc();
        //起始地址
        byte[] startAddress = packet.getStartAddress();
        req[2] = startAddress[0];
        req[3] = startAddress[1];
        //寄存器数量
        int registerNum = packet.getRegisterNum();
        byte[] registerNumBytes = convert.INSTANCE.numToBytes(registerNum);
        req[4] = registerNumBytes[0];
        req[5] = registerNumBytes[1];
        //CRC校验
        byte[] crc = packet.getCrc();
        req[6] = crc[0];
        req[7] = crc[1];

        return ByteBuffer.wrap(req);
    }

    public byte[] encode2Bytes(modbusRequest packet) {
        //modBus请求固定为8字节
        byte[] req = new byte[8];
        //地址码
        req[0] = packet.getAddress();
        //功能码
        req[1] = packet.getFunc();
        //起始地址
        byte[] startAddress = packet.getStartAddress();
        req[2] = startAddress[0];
        req[3] = startAddress[1];
        //寄存器数量
        int registerNum = packet.getRegisterNum();
        byte[] registerNumBytes = convert.INSTANCE.numToBytes(registerNum);
        req[4] = registerNumBytes[0];
        req[5] = registerNumBytes[1];
        //CRC校验
        byte[] crc = packet.getCrc();
        req[6] = crc[0];
        req[7] = crc[1];

        return req;
    }

    /**
     * 目标输出：byte[]{0x30, 0x03, 0x00, 0x08, 0x00, 0x01, 0x01, (byte)0xE9};
     * @param args
     */
    public static void main(String[] args) {
        //构造Packet
        modbusRequest req = new modbusRequest();
        req.setAddress((byte)0x30);
        req.setFunc((byte)0x03);
        req.setStartAddress(new byte[]{(byte)0x00, (byte)0x08});
        req.setRegisterNum(1);
        req.setCrc(new byte[]{(byte)0x01, (byte)0xE9});

        byte[] data = INSTANCE.encode2Bytes(req);
        //测试Packet生成的byte数组
        System.out.println(DatatypeConverter.printHexBinary(data).trim());
        //测试原输入byte数组
        System.out.println(DatatypeConverter.printHexBinary(new byte[]{0x30, 0x03, 0x00, 0x08, 0x00, 0x01, 0x01, (byte)0xE9}).trim());
    }
}
