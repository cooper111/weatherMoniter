package com.example.demo.netty.protocol.model.data;

import lombok.Data;

/**
 * @ClassName modBusData
 * @Description
 * @Company inspur
 * @Author Kevin
 * @Date 2020/7/4 11:57
 * @Version 1.0
 */
@Data
public class modBusData {
    //地址码
    byte address;
    //功能码
    byte func;
    //字节数
    int byteNum;
    //寄存器值
    byte[] registerVal;
    //CRC校验
    byte[] crc;

    public modBusData(){};

    public modBusData(byte address, byte func, int byteNum, byte[] registerVal, byte[] crc) {
        this.address = address;
        this.func = func;
        this.registerVal = registerVal;
        this.crc = crc;
    }
}
