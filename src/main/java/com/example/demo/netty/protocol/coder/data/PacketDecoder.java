package com.example.demo.netty.protocol.coder.data;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @ClassName PacketDecoder
 * @Description
 * @Company inspur
 * @Author Kevin
 * @Date 2020/7/13 22:19
 * @Version 1.0
 */
public class PacketDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        out.add(dataDecoder.INSTANCE.decode(in));
    }
}
