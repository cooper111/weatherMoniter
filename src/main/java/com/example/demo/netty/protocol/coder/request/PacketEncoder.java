package com.example.demo.netty.protocol.coder.request;

import com.example.demo.netty.protocol.model.request.modbusRequest;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @ClassName PacketEncoder
 * @Description
 * @Company inspur
 * @Author Kevin
 * @Date 2020/7/13 22:36
 * @Version 1.0
 */
public class PacketEncoder extends MessageToByteEncoder<modbusRequest> {
    @Override
    protected void encode(ChannelHandlerContext ctx, modbusRequest msg, ByteBuf out) throws Exception {
        requestEncoder.INSTANCE.encode(out, msg);
    }
}
