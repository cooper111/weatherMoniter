package com.example.demo.netty.protocol.model.request;

import lombok.Data;

/**
 * @ClassName modbusRequest
 * @Description
 * @Company inspur
 * @Author Kevin
 * @Date 2020/7/4 10:23
 * @Version 1.0
 */
@Data
public class modbusRequest {
    //地址码
    byte address;
    //功能码
    byte func;
    //起始地址
    byte[] startAddress;
    //寄存器数量
    int registerNum;
    //CRC校验
    byte[] crc;

    public modbusRequest(){};

    public modbusRequest(byte address, byte func, byte[] startAddress, int registerNum, byte[] crc) {
        this.address = address;
        this.func = func;
        this.startAddress = startAddress;
        this.registerNum = registerNum;
        this.crc = crc;
    }
}
