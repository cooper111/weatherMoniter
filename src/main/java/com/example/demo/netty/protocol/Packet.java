package com.example.demo.netty.protocol;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @ClassName Packet
 * @Description 后续如果使用对传输的值需要不同的序列化和反序列化，使用
 * Class<\? extends Packet> requestType = getRequestType(command);//Map里获取传输数据requestType
 * Serializer serializer = getSerializer(serializeAlgorithm);//Map里获取序列化算法serializer
 * serializer.deserialize(requestType, bytes);//反序列化成实例
 * @Company inspur
 * @Author Kevin
 * @Date 2020/7/3 21:47
 * @Version 1.0
 */
@Data
public abstract class Packet {
    /**
     * 协议版本
     */
    @JSONField(deserialize = false, serialize = false)
    private Byte version = 1;


    @JSONField(serialize = false)
    public abstract Byte getCommand();
}