package com.example.demo.test;

import com.example.demo.waiter.entity.TbUser;
import com.example.demo.waiter.mapper.TbUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class TestUser {
    @Autowired
    private TbUserMapper tbUserMapper;

    @Test
    public void selectAll() {
        List<TbUser> users = tbUserMapper.selectList(null);
        users.forEach(c -> log.info("User: {}", c));
    }
}
