package com.example.demo.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @ClassName NettyServerConfig
 * @Description   NettyServer配置       @Value读取自定义配置文件
 * @Company inspur
 * @Author Kevin
 * @Date 2020/6/18 22:19
 * @Version 1.0
 */
@Configuration
@PropertySource({"classpath:nettyServer.properties"})
@Data
public class NettyServerConfig {
    @Value("${tcp.server.port}")
    private String port;
}
